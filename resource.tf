resource "aws_vpc" "vpc_icinga" {
  cidr_block       = "10.10.0.0/16"
 
}
resource "aws_subnet" "subnet-2" {
  vpc_id      = aws_vpc.vpc_icinga.id
  availability_zone = var.subnet2az
  cidr_block        = "10.10.0.0/24"
  
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id      = aws_vpc.vpc_icinga.id
   
}

resource "aws_route_table" "my_rt" {
    vpc_id = aws_vpc.vpc_icinga.id
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.my_igw.id
    }
 
}

resource "aws_security_group" "my_sg" {
     
    vpc_id          = aws_vpc.vpc_icinga.id
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
     ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "80"
        to_port     = "80"
    }
    
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "5665"
        to_port     = "5665"
    }
    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}

resource "aws_route_table_association" "subnet2assoc" {
    subnet_id       = aws_subnet.subnet-2.id
    route_table_id  = aws_route_table.my_rt.id
  
}


resource "aws_instance" "icinga_web" {
    ami                         =  var.icinga
    instance_type               = "t2.micro"
    subnet_id                   = aws_subnet.subnet-2.id
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.icinga_web.public_ip
    }

     provisioner "remote-exec" {
    inline = [
             "sudo apt-get -y update",
             "sudo apt-get install -y apt-transport-https curl",
             # Add the Icinga public signing key to the system.
             " curl https://packages.icinga.com/icinga.key | apt-key add -",
             ## Ubuntu 18.04 / Ubuntu 16.04 

         #   " sudo echo 'deb http://packages.icinga.com/ubuntu icinga-$(lsb_release -sc) main' | sudo tee /etc/apt/sources.list.d/icinga2.list",
          #  " sudo echo 'deb-src http://packages.icinga.com/ubuntu icinga-$(lsb_release -sc) main' | sudo tee -a /etc/apt/sources.list.d/icinga2.list",
            "sudo apt-get update -y",
            "sudo apt-get install -y icinga2",
            "sudo systemctl start icinga2",
            "sudo systemctl enable icinga2" ,
            "sudo apt install firewalld -y",
            "sudo firewall-cmd --zone=public --permanent --add-port=80/tcp  ",
            "sudo firewall-cmd --zone=public --permanent --add-port=5665/tcp" ,
            "sudo firewall-cmd --reload", 
            "sudo apt install mysql-server mysql-client -y | echo 'admin@123' | echo 'OK' |  echo 'admin@123' | echo 'OK' ",
            "sudo systemctl start mysql",
            "sudo systemctl enable mysql",
            "sudo mysql_secure_installation | echo 'admin' | echo 'y' | echo '1' |echo 'n' | echo 'y' | echo 'y' | echo 'y' | echo 'y',
           # "sudo apt install icinga2-ido-mysql -y | echo 'yes' | echo 'yes' | echo 'admin@123' | echo 'OK' |echo 'admin@123' | echo 'OK'",
            "sudo icinga2 feature enable ido-mysql",
            "sudo  systemctl restart icinga2",   
            "sudo apt-get install icingaweb2 libapache2-mod-php icingacli -y",
            "sudo systemctl restart apache2",
            "icingacli setup token create",
            "mysql -u root -p",
            "create database icingaweb2;",
            " grant all privileges on icingaweb2.* to icingaweb2@localhost identified by 'icingaweb2-password';",
            " flush privileges;",
            "quit;"
    ]
  }
  
}
output "vpc_icinga_ip" {
  value = aws_instance.icinga_web.public_ip
}

